# shmArrayAccess

## A class to access shared memory as an array.

It act as a wrapper around the Semaphore functions with an index to store the offset in the same shared memory. To be accessable like an array it implements the `ArrayAccess`, `IteratorAggregate` and `Countable` interfaces.

**./testing/test.php**
```php
<?php
$tsStart    =   microtime(true);
require_once('shmArrayAccess.php');

$shm        =   new shmArrayAccess();
$shm->attachTimeout =   0.5;                    //  Timeout if waiting to attach

$shm->doAttached(function($shm) {               //  do something attached. Overload only works in doAttached
    $shm['test']    =   "test";
    $shm[]          =   1;
    $shm[]          =   (object)    ["4", "5"];
    $shm[]          =   ["4", "5"];
    $shm[]          =   false;
});

$shm['test']    .=  " attach text will work";                   //  attach text will work
$shm[2][0]      =   "overload will not work";                   //  but overload will not work

$shm->doAttached(function($shm) {
    $shm[2][0]      =   "overload will work in doAttached";     //  Overload only works in doAttached
});

var_dump($shm);

echo PHP_EOL."Unset shm -----------------".PHP_EOL.PHP_EOL;
unset($shm);

$shmNew =   new	shmArrayAccess();

var_dump($shmNew);
var_dump($shmNew->stats());

$shmNew->delete();

var_dump(microtime(true) - $tsStart, memory_get_usage(), function_exists('pcntl_fork'));
?>
```
**Note:**
So far it meets my needs, but I'm not sure there are no bugs!