<?php
$tsStart	=	microtime(true);
require_once('shmArrayAccess.php');

//$shmha	=	new	shmArrayAccess();
//$shmha->attach();

$shm	=	new	shmArrayAccess();
$shm->attachTimeout	=	0.5;

$shm->doAttached(function($shm) {
    $shm['test']	=	"test";
    $shm[]			=	1;
    $shm[]			=	(object)    ["4", "5"];
    $shm[]			=	["4", "5"];
    $shm[]			=	false;
});

$shm['test']	.=	" attach text will work";                   //  attach text will work
$shm[2][0]	    =	"overload will not work";                   //  but overload will not work

$shm->doAttached(function($shm) {
    $shm[2][0]	    =	"overload will work in doAttached";     //  Overload only works in doAttached
});

var_dump($shm);

echo PHP_EOL."Unset shm -----------------".PHP_EOL.PHP_EOL;
unset($shm);

$shmNew =   new	shmArrayAccess();

var_dump($shmNew);
var_dump($shmNew->stats());

$shmNew->delete();

var_dump(microtime(true) - $tsStart, memory_get_usage());
?>