<?php
$tsStart	=	microtime(true);
require_once('shmArrayAccess.php');

$shm	=	new	shmArrayAccess();
$shm->attachMaxWait	=	0;

$shm->attach();
$shm['test']	=	"test";
$shm[]			=	1;
$shm[]			=	(object)    ["4", "5"];
$shm[]			=	["4", "5"];
$shm[]			=	false;
$shm->detach();


var_dump($shm);

var_dump($shm->stats());
var_dump(microtime(true) - $tsStart);
?>