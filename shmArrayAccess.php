<?php
/* shmArrayAccess.php - Class for accessing shared memory as array
 * Copyright (C) 2020 Christian Backus
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/* File Authors:
 *   Christian Backus <chrissileinus@googlemail.com>
 */

class	shmArrayAccess	implements	ArrayAccess, IteratorAggregate, Countable {
	private	$shm,	$sem,	$attached,	$key,	$var,	$storage,	$size,	$perm;
	public	$attachTimeout;

	//	__construct	START	-------------------------------------------------------
	public	function	__construct(array $opt = []) {
		$this->key			=	$this->generateKey(@$opt['key']);
		$this->var			=	$this->generateKey(@$opt['var']);
		$this->size			=	intval(@$opt['size'])?:	1024*512;
		$this->perm			=	(isset($opt['perm']) && intval(@$opt['perm']) <= 0666 && intval(@$opt['perm']) >= 0600)?	intval(@$opt['perm']):	0600;

		$this->attachTimeout=	1;

		$this->_init();
	}

	private	function	_init(): void {
		$this->attached		=	false;
		$this->storage		=	[];

		if(!$this->sem		=	sem_get($this->key, 1, $this->perm, 1))				throw new Exception( __CLASS__ . __FUNCTION__ . "::sem object are not initialized");
		if(!$this->shm		=	shm_attach($this->key, $this->size, $this->perm))	throw new Exception( __CLASS__ . __FUNCTION__ . "::shm object are not initialized");
	 }
	//	__construct	END		-------------------------------------------------------

	//	__destruct	START	-------------------------------------------------------
	public	function	__destruct() {
		@sem_release($this->sem);
		@shm_detach($this->shm);
	}
	//	__destruct	END		-------------------------------------------------------

	//	generateKey	START	-------------------------------------------------------
	private	function	generateKey($name) {
		return	array_sum(unpack('I4int', md5(__CLASS__.$name, true)));	//	generate a integer key out of all bits of the md5 result of $name
	}
	//	generateKey	END		-------------------------------------------------------

	//	error	START	-----------------------------------------------------------
	private	function	error($message, $type = E_USER_NOTICE): void {
		$debug_backtrace	=	debug_backtrace();
		do {
			$caller = next($debug_backtrace);
		} while(@$caller['file'] == __FILE__);
		trigger_error($message.' in '.$caller['function'].' called from '.@$caller['file'].' on line '.@$caller['line']." error handler", $type);		
	}
	//	error	START	-----------------------------------------------------------

	
	//	stats		START	-------------------------------------------------------
	public	function	stats() {
		$shmop	=	shmop_open($this->key, 'a', 0, 0);
		$stats	=	unpack("Qused/Qfree/Qtotal", shmop_read($shmop, 8+PHP_INT_SIZE, 3*PHP_INT_SIZE));
		shmop_close($shmop);
		return	$stats;
	}
	//	stats		END		-------------------------------------------------------


	//	Semaphor	START	-------------------------------------------------------
	public	function	attach($timeout = false) {
		$timeout	=	(float)	$timeout?: $this->attachTimeout;
		if(!$this->attached) {
			if($timeout === false)
				$this->attached	=	sem_acquire($this->sem);
			else {
				$end	=	$timeout + microtime(true);
				do {
					$this->attached	=	sem_acquire($this->sem, true);
				} while($end > microtime(true) && !$this->attached);
			}
			if($this->attached) {
				$this->read();
			}
			else {
				$this->error(__CLASS__ . __FUNCTION__ . "::attach timeout ({$timeout} sec.)");
			}
		}
		return	$this->attached;
	}

	public	function	detach(): void {
		$this->write();
		@sem_release($this->sem);
		$this->attached	=	false;
	}

	public	function	doAttached(callable $do, $wait = false) {
		$result	=	null;
		$lastAttachedState	=	$this->attached;
		if($this->attach($wait)) {

			$result	=	$do($this);

		}
		if(!$lastAttachedState)	$this->detach();
		return	$result;
	}
	//	Semaphor	END		-------------------------------------------------------

	//	SHM handling	START	---------------------------------------------------
	public	function	delete(): void {
		@sem_remove($this->sem);
		@shm_remove($this->shm);
	}

	private	function	isset(): bool {
		return	shm_has_var($this->shm, $this->var);
	}

	private	function	read() {
		$this->storage	=	@shm_get_var($this->shm, $this->var)?:	[];
	}

	private	function	write(): bool {
		return	shm_put_var($this->shm, $this->var, $this->storage);
	}

	public	function	unset(): void {
		shm_remove_var($this->shm, $this->var);
	}
	//	SHM handling	END		---------------------------------------------------

	//	ArrayAccess	START	-------------------------------------------------------
	public	function	offsetExists($offset) {
		return	$this->doAttached(function() use (&$offset) {
			return	isset($this->storage[$offset]);
		});
	}

	public	function	&offsetGet($offset) {
		if($this->attached)	return	$this->storage[$offset];

		$result	=	$this->doAttached(function() use (&$offset) {
			return	$this->storage[$offset];
		});
		return	$result;
	}

	public	function	offsetSet($offset, $value): void {
		$this->doAttached(function() use (&$offset, &$value) {
			if(is_null($offset)) {
				$this->storage[]		=	$value;
			}
			else {
				$this->storage[$offset]	=	$value;
			}
		});
	}

	public	function	offsetUnset($offset): void {
		$this->doAttached(function() use (&$offset) {
			unset($this->storage[$offset]);
		});
	}
	//	ArrayAccess	END		-------------------------------------------------------

	//	IteratorAggregate	START	-----------------------------------------------
	public	function	getIterator() {
		$storage	=	[];
		$this->doAttached(function() use (&$storage) {
			$storage	=	$this->storage;
		});
		return	new	ArrayIterator($storage);
	}
	//	IteratorAggregate	END		-------------------------------------------------------

	//	Countable	Start	-------------------------------------------------------
	public	function	count() {
		return	$this->doAttached(function() {
			return	count($this->storage);
		});
	}
	//	Countable	END		-------------------------------------------------------

	//	__debugInfo	Start	-------------------------------------------------------
	public	function	__debugInfo() {
		$storage	=	[];
		$this->doAttached(function() use (&$storage) {
			$storage	=	$this->storage;
		});
		return	$storage;
	}
	//	__debugInfo	END		-------------------------------------------------------
}
?>